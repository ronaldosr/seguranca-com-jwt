package br.com.ronaldosr.seguranca.config;

import br.com.ronaldosr.seguranca.auth.FiltroAutenticacao;
import br.com.ronaldosr.seguranca.auth.FiltroAutorizacao;
import br.com.ronaldosr.seguranca.auth.JWTUtil;
import br.com.ronaldosr.seguranca.model.Usuario;
import br.com.ronaldosr.seguranca.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class SegurancaConfig extends WebSecurityConfigurerAdapter {

//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        // Libera acesso para o Post na rota. Todas as outras requisições necessitam de autenticação
//        http.authorizeRequests()
//                .antMatchers(HttpMethod.POST, "/usuarios").permitAll()
//                .antMatchers(HttpMethod.GET, "/usuarios").permitAll()
//            .anyRequest().authenticated();
//    }

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UsuarioService usuarioService;

    private static final String[] ENDERECOS_PUBLICOS_METHODO_POST = {
            "/usuarios",
            "/login"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // Desabilita a validação de CSRF, permitindo que as requisições enviadas à rota abaixo sejam executadas
        // Ele é usado para formulários HTML e CSS. No caso da API, não é necessário, uma vez que trabalhamos
        // com requisições que não guardam estado e transmitem os dados organizados em JSON
        http.csrf().disable();

        // Utilização do CORS customizado
        http.cors();

        // Informa que a API não guarda estados
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        // Lê um vetor com as rotas autorizadas a receber requisições. As rotas não liberadas são bloqueadas.
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, ENDERECOS_PUBLICOS_METHODO_POST).permitAll()
                .anyRequest().authenticated();

        // Adicionar o filtro de autenticação na requisição Http
        http.addFilter(new FiltroAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroAutorizacao(authenticationManager(), jwtUtil, usuarioService));
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usuarioService).passwordEncoder(construtorEncoder());
    }

    @Bean
    CorsConfigurationSource construtorCors() {
        // Construção de CORS para habilitar requisições de qualquer domínio
        UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        // Exemplo de path: "www.site.com.br/"**", "/site/**"
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return cors;
    }

    @Bean
    BCryptPasswordEncoder construtorEncoder() {
        // Criptografia de senhas
        return new BCryptPasswordEncoder();
    }
}
