package br.com.ronaldosr.seguranca.repository;

import br.com.ronaldosr.seguranca.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByEmail(String email);
}
