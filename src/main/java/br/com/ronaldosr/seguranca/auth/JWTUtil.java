package br.com.ronaldosr.seguranca.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "dukqvy3t39834bef87w";
    private static final Long TEMPO_DE_VALIDADE = 86000l;

    public String gerarToken(String email) {
        // Calcula a data de vencimento do token
        Date dataDeVencimento = new Date(System.currentTimeMillis() + TEMPO_DE_VALIDADE);

        // Montagem do token. Utiliza os parâmetros:
        // 1. setSubject para inclusão da chave de identificação
        // 2. setExpiration para determinar a data de vencimento do token
        // 3. signWith para determinar o algorítimo usado na assinatura do token
        //    e SEGREDO.getBytes() para montar a chave de hash
        // 4. compact() para montar o token
        String token = Jwts.builder().setSubject(email)
                .setExpiration(dataDeVencimento)
                .signWith(SignatureAlgorithm.HS512, SEGREDO.getBytes()).compact();
        return token;
    }

    // Métodos para validação do token
    public String getEmail(String token) {
        Claims claims = getClaims(token);
        String email = claims.getSubject();
        return email;
    }

    public Claims getClaims(String token) {
        return Jwts.parser().setSigningKey(SEGREDO.getBytes()).parseClaimsJws(token).getBody();
    }

    public boolean tokenValido(String token) {
        Claims claims = getClaims(token);
        String email = claims.getSubject();

        Date dataExpiracao = claims.getExpiration();
        Date dataAtual = new Date(System.currentTimeMillis());

        if (email != null && dataExpiracao != null && dataAtual.before(dataExpiracao)) {
            return true;
        } else {
            return false;
        }
    }
}
