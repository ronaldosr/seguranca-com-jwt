package br.com.ronaldosr.seguranca.auth;

import br.com.ronaldosr.seguranca.DTO.LoginDTO;
import br.com.ronaldosr.seguranca.service.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

public class FiltroAutenticacao extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;
    private JWTUtil jwtUtil;

    public FiltroAutenticacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {

        // Transformar objetos em JSON ou JSON em objetos
        ObjectMapper objectMapper = new ObjectMapper();

        try {
            // Lê os dados da requisição de login enviadas no InputStream e salva dentro da classe LoginDTO
            LoginDTO loginDTO = objectMapper.readValue(request.getInputStream(), LoginDTO.class);

            // Cria o token
            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    loginDTO.getEmail(), loginDTO.getSenha(), new ArrayList<>());

            // Autentica o token
            Authentication authentication = authenticationManager.authenticate(authToken);
            return authentication;

        } catch (IOException e) {
            // Capturar os erros de entrada/saída do ObjectMapper e lança RuntimeException
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                            FilterChain chain, Authentication authResult)
            throws IOException, ServletException {
        // Captura o e-mail usado para identificação do usuário
        String email = ((UsuarioAuth) authResult.getPrincipal()).getUsername();

        // Geração do token
        String token = jwtUtil.gerarToken(email);

        response.addHeader("Authorization", "Bearer " + token);
    }
}
