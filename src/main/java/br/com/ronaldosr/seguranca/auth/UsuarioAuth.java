package br.com.ronaldosr.seguranca.auth;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class UsuarioAuth implements UserDetails {

    private long id;
    private String email;
    private String senha;

    public UsuarioAuth() {
    }

    public UsuarioAuth(long id, String email, String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // Determina as credenciais do usuário
        return null;
    }

    @Override
    public String getPassword() {
        // Recebe a senha
        return this.senha;
    }

    @Override
    public String getUsername() {
        // Recebe e-mail como username
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        // Seta a conta como "expirada"
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // Set a conta como "desbloqueada"
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // Seta a credencial como "expirada"
        return true;
    }

    @Override
    public boolean isEnabled() {
        // Seta usuário como "habilitado"
        return true;
    }
}
