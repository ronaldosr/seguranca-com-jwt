package br.com.ronaldosr.seguranca.service;

import br.com.ronaldosr.seguranca.auth.UsuarioAuth;
import br.com.ronaldosr.seguranca.model.Usuario;
import br.com.ronaldosr.seguranca.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    public Usuario salvarUsuario(Usuario usuario) {
        String senha = usuario.getSenha();
        usuario.setSenha(encoder.encode(senha));

        return usuarioRepository.save(usuario);
    }

    public List<Usuario> listarTodosUsuarios() {
        return usuarioRepository.findAll();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // Recupera o e-mail cadastrado para o usuário
        Usuario usuario = usuarioRepository.findByEmail(email);
        if (usuario == null) {
            throw new UsernameNotFoundException("E-mail não cadastrado");
        }
        UsuarioAuth usuarioAuth = new UsuarioAuth(usuario.getId(), usuario.getEmail(), usuario.getSenha());
        return usuarioAuth;
    }
}
